{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Regression\n",
    "\n",
    "Now we will start to deal with *regression*. Regression refers to a problem where we want to produce a model that relates two variables to each other. Typically we talk about *independent* and *dependent* variables; you can also use the terms *predictor* and *response*, or others as you see fit and depending on the context of your problem.\n",
    "\n",
    "A general sort of regression problem can be written as follows:\n",
    "$$\n",
    "\\min_{\\beta} \\Vert Y - f(X, \\beta) \\Vert\n",
    "$$\n",
    "\n",
    "Here $Y$ are the measured dependent variables, $X$ are the measured independent variables, $\\beta$ are the unknown parameter values that define our specific model, and $f$ is the model itself, i.e. a way to produce predictions out of independent variables and model parameters. The symbol $\\Vert \\cdot \\Vert$ denotes a *norm*, i.e. a way of measuring how far off the model predictions $f(X, \\beta)$ are from the true outcomes $Y$. The form of the function $f$, as well as the properties of the data $X$ and $Y$, determines the type of regression problem at hand.\n",
    "\n",
    "We will focus especially on *linear regression*, i.e. cases in which the function $f$ depends linearly on the parameters $\\beta$. Such problems can be written in the form:\n",
    "$$\n",
    "\\min_{\\beta} \\Vert Y - A(X)\\beta \\Vert\n",
    "$$\n",
    "where $A(X)$ is a matrix that depends on the data $X$. Note that such a model is linear even when the matrix $A$ depends nonlinearly on the data $X$. That is to say, it is generally not the case that $A(X_1 + X_2) = A(X_1) + A(X_2)$. The matrix $A$ has to have a specific shape: it should have as many rows as the length of $Y$, and as many columns as the length of $\\beta$. Generally, each column of $A$ will consist of some function applied to all the entries of $X$. We sometimes use the word *feature* for such functions. The linear regression problem tries to approximate the outputs $Y$ by a linear combination of features of $X$, where $\\beta$ is the vector of coefficients of those features.\n",
    "\n",
    "This form may seem restrictive, but it is in fact extremely general and will essentially take us all the way to the end of the course. To see how general this form really is, the following exercise will have you write down the matrix $A(X)$ for polynomial regression and for Fourier series.\n",
    "\n",
    "# Exercise 1\n",
    "## Part a: pencil and paper\n",
    "### Polynomial regression\n",
    "Suppose you have data $X = (x_1, x_2, \\dots, x_n)$ and $Y=(y_1, y_2, \\dots, y_n)$ where $x_i, y_i \\in \\mathbb{R}$. You suspect that the $y$'s can be written as a polynomial function of the $x$'s with degree at most $m$, i.e.\n",
    "$$\n",
    "y = c_0 + c_1 x + c_2 x^2 + \\dots + c_m x^m\n",
    "$$\n",
    "write down a matrix $A$, which will depend on the data $X$, such that the vector of coefficients $c = (c_0, c_1, \\dots, c_m)$ is the solution to the regression problem $\\min_{c} Y - Ac$. What is the shape of $A$?\n",
    "\n",
    "### Fourier series\n",
    "Suppose you have data $X = (x_1, x_2, \\dots, x_n)$ and $Y=(y_1, y_2, \\dots, y_n)$ where $x_i, y_i \\in \\mathbb{R}$. You suspect that the $y$'s can be written as a Fourier cosine series $x$'s with known base frequency $\\omega$ and up to $m$ harmonics, i.e.\n",
    "$$\n",
    "y = c_0 + c_1 \\cos(\\omega x) + c_2 \\cos(2\\omega x) + \\dots + c_m \\cos(m\\omega x)\n",
    "$$\n",
    "write down a matrix $A$, which will depend on the data $X$, such that the vector of coefficients $c = (c_0, c_1, \\dots, c_m)$ is the solution to the regression problem $\\min_{c} Y - Ac$. What is the shape of $A$?\n",
    "\n",
    "***Remark***: when $m = n-1$, the matrices you found above are referred to as Vandermonde and Discrete Cosine Transform matrices, respectively.\n",
    "\n",
    "## Part b: coding\n",
    "For each of the above subparts (polynomial regression and Fourier series), write a function that takes as input a data vector $X$ and returns the corresponding feature matrix $A(X)$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "((100,), (100,))"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "import numpy as np\n",
    "\n",
    "def polynomial_feature_matrix(X, m = 1):\n",
    "    \"\"\"\n",
    "    computes the matrix A(X) for use in polynomial regression\n",
    "\n",
    "    INPUT:\n",
    "        X = numpy array of length n giving measurements of the independent variable\n",
    "        m = highest power to raise X to\n",
    "\n",
    "    OUTPUT:\n",
    "        A = matrix of monomial features of X up to order m\n",
    "    \"\"\"\n",
    "    return np.empty_like(X)\n",
    "\n",
    "def cosine_feature_matrix(X, m = 1, w = np.pi):\n",
    "    \"\"\"\n",
    "    computes the matrix A(X) for use in Fourier cosine series regression\n",
    "\n",
    "    INPUT:\n",
    "        X = numpy array of length n giving measurements of the independent variable\n",
    "        m = highest harmonic to include\n",
    "        w = base frequency\n",
    "\n",
    "    OUTPUT:\n",
    "        A = matrix of cosine features of X up to the m-th harmonic of frequency w\n",
    "    \"\"\"\n",
    "    return np.empty_like(X)\n",
    "\n",
    "# test your functions\n",
    "# be sure to try out other values of n, m, and w to make sure they work as intended\n",
    "\n",
    "n = 100\n",
    "m = 10\n",
    "X = np.random.rand(n)\n",
    "\n",
    "A1 = polynomial_feature_matrix(X, m)\n",
    "A2 = cosine_feature_matrix(X, m)\n",
    "\n",
    "# check the shapes of these matrices to make sure they agree with your answers above\n",
    "A1.shape, A2.shape"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercise 2\n",
    "In this exercise you will demonstrate one of the main reasons that least-squares regression is so popular, namely that it can be carried out by inverting a small matrix (of size equal to the number of model terms, as opposed to the number of data points)\n",
    "\n",
    "Suppose that you've created a matrix $A(X)$ as described above. Each column corresponds to a function of the independent variable $x$, and each row corresponds to one of the measurements, $x_i$. For the sake of generality, denote the entries of the matrix $A$ as $a_{ij}$, where $i=1\\dots n$, $n$ being the number of measurements, and $j=1\\dots m$, $m$ being the number of features.\n",
    "\n",
    "## Part a: Gradient of the loss function\n",
    "Let $E(\\beta)$ denote the least-squares loss function, i.e.\n",
    "$$\n",
    "\\begin{align}\n",
    "E(\\beta) &= \\sum_{i=1}^{n} {1\\over 2} (y_i - (A \\beta)_i)^2\\\\\n",
    "&= \\sum_{i=1}^{n} {1\\over 2} \\left(y_i - \\sum_{j=1}^{m}a_{ij}\\beta_j\\right)^2\\\\\n",
    "\\end{align}\n",
    "$$\n",
    "Show that the gradient of the loss function with respect to $\\beta$ is given by:\n",
    "$$\n",
    "\\nabla E(\\beta) = -A^T (y - A\\beta)\n",
    "$$\n",
    "\n",
    "## Part b: Simplifying the optimality condition\n",
    "We know that at the minimum, the gradient of the loss function is equal to zero. This gives the equation\n",
    "$$\n",
    "\\nabla E(\\beta) = 0 \\implies A^T A \\beta = A^T y\n",
    "$$\n",
    "\n",
    "What is the shape of the matrix $A^T A$? What is the meaning of its entries?\n",
    "\n",
    "Assuming that $A^T A$ is invertible (which is true as long as $A$ is full rank), we can simply write down the optimal solution:\n",
    "$$\n",
    "\\hat{\\beta} = (A^TA)^{-1} A^T y\n",
    "$$\n",
    "\n",
    "## Part c: Coding\n",
    "\n",
    "Using the functions you wrote in Exercise 1, do the following:\n",
    "* Compute the feature matrices ($A$) on some simulated data\n",
    "* Compute $A^T A$ and inspect it (use a small $m$, say less than 5, so that you can read the matrix easily). Can you notice any differences between the polynomial case and the cosine series case?\n",
    "* Generate some random output data $Y$ and use `scipy.linalg.inv` to invert $A^T A$ and get the least-squares solution to $Y = A(X)\\beta$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 100\n",
    "m = 5\n",
    "w = np.pi\n",
    "\n",
    "X = np.random.rand(n)\n",
    "Y = np.random.rand(n)\n",
    "\n",
    "A1 = polynomial_feature_matrix(X, m)\n",
    "A2 = cosine_feature_matrix(X, m, w)\n",
    "\n",
    "# your code here"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercise 3 (coding)\n",
    "Next, you will perform polynomial interpolation on data that truly do obey a polynomial relationship, corrupted by noise. To do this, follow the same procedure outlined in Exercise 2, part c above.\n",
    "\n",
    "See how your computed results compare to the true function. How do they depend on the noise level? On the maximum polynomial degree you specify?\n",
    "\n",
    "What do you get if you do cosine series regression on the same data?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<matplotlib.collections.PathCollection at 0x1ea0c8507c0>"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAiMAAAGdCAYAAADAAnMpAAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjYuMiwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8o6BhiAAAACXBIWXMAAA9hAAAPYQGoP6dpAABOZ0lEQVR4nO3de1xUdf4/8NfMcBOSQUKZwUjw0gURFDcMzS6GSbiUW+2artq65ablfkt3v5tUiq5taFu57XorNqtd17R7WXynEHMtw/glskpY3sZLOoMXFBDk4sz5/eEOgYCcc+acmTMzr+fjwWO34fM5vI8DnDefy/ujEwRBABEREZGX6L0dABEREQU2JiNERETkVUxGiIiIyKuYjBAREZFXMRkhIiIir2IyQkRERF7FZISIiIi8iskIEREReVWQtwMQw+l04vjx4+jZsyd0Op23wyEiIiIRBEFAXV0d4uLioNd3Pf7hE8nI8ePHER8f7+0wiIiISIajR4/iqquu6vLzPpGM9OzZE8DFm4mMjPRyNERERCRGbW0t4uPjW5/jXfGJZMQ1NRMZGclkhIiIyMd0t8SCC1iJiIjIq5iMEBERkVcxGSEiIiKvYjJCREREXsVkhIiIiLyKyQgRERF5FZMRIiIi8iomI0RERORVPlH0TA0Op4BSazVO1DWiT88wpCdGw6DnuTdERESeFpDJiKXChkUbK2GraWx9zWwMQ15OErKSzV6MjIiIKPAE3DSNpcKGWWvL2iUiAGCvacSstWWwVNi8FBkREVFgCqhkxOEUsGhjJYROPud6bdHGSjicnbUgIiIiNUhORrZu3YqcnBzExcVBp9Phgw8+6LbPli1bkJaWhtDQUAwcOBCvv/66jFDdV2qt7jAi0pYAwFbTiFJrteeCIiIi8jCHU8AXe09izvqd+M0/vkHB1gNovuD0WjyS14zU19cjNTUVv/71r3HPPfd0295qtWL8+PGYOXMm/vWvf6G4uBgPPfQQzGYzxo0bJytouU7UdZ2IyGlHRETkawp32TD37XI0tvyYfHxWWYU/FX6HGaMT8dT4JI/HJDkZufPOO3HnnXeKbr969WokJibihRdeAABcf/31+PLLL7Fs2TKPJyN9eoaJahdzRajKkRAREXlefmElXt5q7fLzBV9Yceh0PQqm3eDBqDywZqSkpASZmZntXhs3bhxKSkq67NPU1ITa2tp2H0oY3q8XxOzenbthJxeyEhGRXyncdfyyiYhLUeUJfFx+3AMR/Uj1ZMRutyM2Nrbda7GxsaitrcX58+c77ZOfnw+j0dj6ER8fr0gsOw6fgZi1qVV1zdxZQ0REfsPhFPD0hxWi2//unXKPbubQ5G6a3Nxc1NTUtH4cPXpUkevaazpPfjojgDtriIjIP5Raq1Fd3yK6fdMFAdsPnlYxovZUT0ZMJhOqqqravVZVVYXIyEj06NGj0z6hoaGIjIxs96GE6vpmSe25s4aIiPyBnI0Z2/afUiGSzqmejGRkZKC4uLjda0VFRcjIyFD7S3cQLWNhKnfWEBGRrxO7gaOt42fFzya4S3Iycu7cOZSXl6O8vBzAxa275eXlOHLkCICLUyzTpk1rbT9z5kwcPHgQf/jDH/Ddd99h5cqVeOuttzBnzhxl7kACU6T0N0POG0hERKQl6YnRMBulPc/6RnU+e6EGycnIN998g2HDhmHYsGEAgLlz52LYsGFYsGABAMBms7UmJgCQmJiITz75BEVFRUhNTcULL7yAv//97x7f1gtcfDOiI0JEtw8PNiA9MVrFiIiIiNRn0OtwV6q0s9dGDoxRKZqOdIIgaH6FZm1tLYxGI2pqatxeP1K4y4ZH1pWJanvPsDi8OHGYW1+PiIjI2xxOATct3XzZKuRt9Qg2oGLROLdPsxf7/Nbkbho1ZaeY8eBNCaLavrfzOPILK9UNiIiISGXdHYdyqefuS3E7EZEi4JIRAJj/08G4/breotq+vNXKhISIiHxaUaVddNuxSX2QkxqnYjQdBWQyAgC/Gpkoum3BF1avHiBEREQkl8Mp4AORFVXHDzF5vBQ8EMDJyLtlP4hu6xSAf5YcUi8YIiIilSzfvE9Una2IED3+OinNAxF1FLDJSEPzBUntrafrVYqEiIhIHZYKG5Zt2ieqrQDPrRG5VMAmIzckXCmpvffeIiIiIukcTgGLNopf89jQ7PBa1fGATUYeGJkgKcEYFt9LtViIiIiUJnUHDeC9quMBm4yEBOnx0Gjxi1jNHqxER0RE5K7PvhW/g8bFW1XHAzYZAYCnxich83pxW3w3f1fVfSMiIiINcDgFvCNhowYAmI1hXqs6HtDJCAD8/YF0UUXQCr5gvREiIvINpdZq1DVK26hx/w1Xe7TQWVsBn4wAwBNZ14tq98pWK843O1SOhoiIyD1y1n4kxISrEIk4TEYgvoaIAGDY4s9gqbCpGg8REZE75Kz98OYp9UxGAByubhDdtrHFiVlry5iQEBGRZqUnRiMyzCC6fVSPYK+eUs9kBEC/aGlDUwKARRsr4XBq/sBjIiIKQM9Z9qC2UfyygumjEry2XgRgMgIAmJohreYIANhqGr1WHIaIiKgrhbtseHmrVXT7iBADZo8ZpGJE3WMygos1R35zs/iaIy7eKg5DRETUGYdTwNMfVkjq85ub+3t1VARgMtIqNzsJMyQUQQO8u9iHiIjoUqXWalGH4rloYVQEYDLSzlPjkzD71v6i2vYK9+5iHyIiokv9/YsDktr/5uYBXh8VAZiMdJAxQFxF1mkZ/TTxBhIREQEX14oUf3dSdPuoHkGYPWagihGJx2TkEqfqm0S169/7CpUjISIiEsfhFPD0B9LWiiy5N0Uzf1QzGbmE2HUgMVeEqhwJERGROKXWalQ3iF8rMidzELKSzSpGJA2TkUukJ0bDbAzrdqvvjH/8PxY+IyIiTdhUKe2E3oSYCJUikYfJyCUMeh3ycpLQXTmzhmYnZrISKxEReZnDKeD98mOS+mhtNyiTkU6MTTIhqkewqLYLP/qWlViJiMhrLm7nbRHdPjxYr7ndoExGOlFqrcbZ8+LeWHttEyuxEhGR1xw/I/58NQD4+U+u0szCVRcmI52QWlnVXstKrERE5HmWChsWflwpqU9WcpxK0cgX5O0AtEjqXFr1OXHbgYmIiJRiqbBh1tqybtc4tmU2hmluigbgyEin0hOjYRS5ZgQAoiNCVIyGiIioPYdTwKKNlZISEQDIy0nS3BQNwGSkUwa9Dr8eJf6cmgMn61Fy4DQXshIRkUeUWqthqxG/RECvA1ZOHqap2iJtMRnpwuwxA0XvqFn++X5MKtiOm5Zu5lZfIiJSnb3mvKT2yyelITtFe2tFXJiMdMGg12HJvUMk9bHXNGIWa48QEZHKtu0/Jam9XuNPe42H511ZyWasnDwMOpHTa65JmkUbKzllQ0REqnA4BRTtqZLUR+vPJSYj3egVEQpBwvsnALDVNLL2CBERqaLUWo2a8xck9dH6c4nJSDekzsu5SK1VQkREJIbc54uWn0tMRrpRXS/+FMS2YiJ4qi8RESlP7rkyWjuPpi0mI92IvkJeUvH/Dp1WOBIiIqKLtbCiwsXXwgK0W+zMhclIN0yR8jLJ1786rOnFQkRE5LvONzsktddqsTMXJiPdSE+MlpWQnD3founFQkRE5JseX1+GpgtO0e3nZF6j2WJnLkxGumHQ67DwriRZfbW8WIiIiHxP8wUnPt5lF93eFBmK2WMGqhiRMpiMiJCVbMavRyVI7qflxUJEROR7/llySNJ5NJPS+2l6esaFyYhIY5NMktprfbEQERH5nkOnGyS1T4gJVykSZTEZESk9MRpmo/iRjp+mmHwiGyUiIt9gqbDh/bKjkvr4ygg9kxGRDHod8nLErx0p+OIQz6ghIiJFWCpsmLm2DOeaxS9c9aUReiYjEmQlm/GghLUjWj8LgIiItM/hFDD3rf9I6qOD9rfztsVkRKJMCWtHtH4WABERad9X+06hQUJdEbMxDKumpGl+O29bQd4OwNe46o7Ya8Vt2y2qtCNjwJUqR0VERP7q3Z0/iGpnAPCPB0fgxgFX+syIiAtHRiQy6HWYlH616PZrtnHtCBERyXfw5DlR7RwA9HqdzyUiAJMRWaRuleLaESIiksPhFLDvhLhkBJB/0ry3MRmRQepWKa4dISIiObYfOI3zLeJ30Mg9ad7bmIzIkJ4YjegIaScmHj/rm9kqERF5h6XChkfXlUnqI/ekeW9jMiKDQa/DM3cnS+pTfvSMStEQEZG/sVTYMGttGc6eb5HUT+5J897GZESm7JQ4PHxzorfDICIiP+NwCli0sVLSGTSAbxU5uxSTETfkZifh3rS+otomXBmhcjREROQPSq3VsNVIP/X9/huu9smdNACTEbfl35OC7t57vQ6YmpHgkXiIiMi3naiTnogAvnMoXmdkJSMrVqxAQkICwsLCMGLECJSWll62/V/+8hdce+216NGjB+Lj4zFnzhw0Nsr7x9aakCA9Zoy+/HRN9hCzz2arRETkWTEyF6H6yqF4nZGcjGzYsAFz585FXl4eysrKkJqainHjxuHEiROdtl+3bh3mzZuHvLw87NmzB6+++io2bNiAJ5980u3gtSI3OwkP35zY5QjJx7tsSFtchJc27WW9ESIiujwZj4moHsE+u14EkJGMvPjii5gxYwamT5+OpKQkrF69GuHh4VizZk2n7b/66iuMGjUKkydPRkJCAu644w5MmjSp29EUX5ObnYS/ThyKsODO/0lrzrdg2aZ9GP5MESuyEhFRl07VN0nuM31Ugk+PwEtKRpqbm7Fjxw5kZmb+eAG9HpmZmSgpKem0z8iRI7Fjx47W5OPgwYMoLCxEdnZ2l1+nqakJtbW17T60zlJhw+z15WjspjjN2YYWzFxbxoSEiIg6FRMhbZrmitAgzB4zSKVoPENSMnLq1Ck4HA7Exsa2ez02NhZ2u73TPpMnT8Yf//hH3HTTTQgODsaAAQNw6623XnaaJj8/H0ajsfUjPj5eSpge59qGJUXue7s5ZUNERB1JHOB47t4Unx4VATywm2bLli149tlnsXLlSpSVleG9997DJ598gsWLF3fZJzc3FzU1Na0fR48eVTtMt8jZhnWmoQXbD55WKSIiIvJVp86Jn6YZc11vZKeYVYzGM4KkNI6JiYHBYEBVVVW716uqqmAymTrtM3/+fEydOhUPPfQQAGDIkCGor6/Hb37zGzz11FPQ6zvmQ6GhoQgN9Z2StnK3Yf2z5DBGDYxROBoiIvJlh07Vi247Y/QAFSPxHEkjIyEhIRg+fDiKi4tbX3M6nSguLkZGRkanfRoaGjokHAaDAQAgCP4xTSF3O5XlWzvXjhARUStLhQ3LNu0T1dYUGerTO2jakjxNM3fuXBQUFOCNN97Anj17MGvWLNTX12P69OkAgGnTpiE3N7e1fU5ODlatWoX169fDarWiqKgI8+fPR05OTmtS4uvSE6NhNspLSOZx7QgREQFovuDEk+/vFt1+4V2DfX6tiIukaRoAmDhxIk6ePIkFCxbAbrdj6NChsFgsrYtajxw50m4k5Omnn4ZOp8PTTz+NY8eOoXfv3sjJycGf/vQn5e7Cywx6HfJykjBrbZnk7eFnG1qwfPN+PJbp2yuhiYhIPkuFDU++X4Hq+u4PxrsiNAjP/zwFWcm+v1bERSf4wFxJbW0tjEYjampqEBkZ6e1wumSpsOF3b/8H9U0OSf2iwoOx4+mxfpPhEhGReK4TesU+jJf9IhU/S7tK1ZiUIvb5zbNpFJSVbMavRiZI7ne2oQWl1mrlAyIiIk2Tc0KvydhDtXi8hcmIwkb2l7c7Ru6OHCIi8l1SS0NER/h22feuMBlR2I0DrkRUeLDkfr58wBEREckj9Q/Rnw3t65dT+kxGFGbQ67DkniGS+uh1wPB+vVSKiIiItErqH6KZSZ3X9PJ1TEZUkJVsxsrJw0S3dwrAjsNnVIyIiIi0aHi/XuglcjTdbAzzyykagMmIasYlmxHVQ/zOaa4ZISIKLJYKG9Kf3YQzDd1v5wWA+eOT/HKKBpBRZ4TEKbVW4+z5C6LbHzrVoGI0RESkJZYKG2auLZPUp1dEiErReB9HRlQidaRjzZcHsW3fKVZjJSLycw6ngIUfSTvpHfDvEXQmIyqRuiippvECfvnq17hp6WaeV0NE5MdKrdWw10pPLPx51yWTEZW4zquROrtnq2nErLVlTEiIiPxUUaVdch9/XrwKMBlRjeu8GjkEAIs2VnLKhojIz1gqbFiz7ZDkfvPHX++3i1cBJiOqyko2Y9WUNJgiQyX3tdU0skQ8EZEfcZV+l6NXhPTniC9hMqKyrGQzts27Hfel9ZXc158XKxERBRqppd/b8vfnAZMRDzDodRh9TW/J/fx5sRIRUaBxJ6Hw9+cB64x4iNRvJH9frEREFGjkJBQ6AKYAeB5wZMRD0hOjEREi/p87L8d/K+0REQWi9MRo9AwTPwbgegIEwvOAyYiHGPQ63HxNH9HtnU4VgyEiIo8z6HUYfrX4Q1FNxjCsmpKGrGSzilFpA5MRD5pyYz/RbR9Zx1ojRET+xOEUsOOIuENRp954Nb58YkxAJCIAkxGPurH/lQgPMYhun/vebtYaISLyE6XWatQ1ijuzLO3qXn4/NdMWkxEPMuh1ePjmAaLbn2lowd+K96kYEREReUrBFwdEtzUZe6gYifYwGfGw2WMGShodeal4Hwp3HVcxIiIiUlvhLhs2f3dSVNvoiGC/3z1zKSYjXhASJP6fXQDwyLqdXD9CROSjHE4Bf3j3P6LbP3N3ckBN0QBMRjyu1FqNsw0tkvvxrBoiIt/0+PoynGtyiGqbFm9EdkqcyhFpD5MRD5NbgY9n1RAR+Z7CXcexcZf4U3pHDoxRMRrtYjLiYe6U9PX3swmIiPyJwyng6Q8rJPXJ6M9khDwgPTEaZmMY5MwGHjrVoHg8RESkjlJrNarrpU3L3xBgC1ddmIx4mEGvQ15OEgBITkiWbdrLhaxERD5iU6X46RmXHYfFFUXzN0xGvCAr2YxVU9JgMkqfsuFCViIi7XM4Bbxffkxyv0Cdjmcy4iVZyWZ8+cQY/OuhETBIGCLhQlYiIu2TM0UDuLeu0JcxGfEig16HG/tfiWAJdUeAwM2ciYh8RZGMKRqzMSzgip25MBnxslJrNRpbpB3RG3NFqErREBGRuywVNqzZdkhyv7ycpIArdubCZMTLZI1ycMkIEZEmOZwCFm2slNzvwVEJAXNCb2eYjHiZnPnBU/VNKkRCRETuKrVWw1Yj/Y/MzCSTCtH4DiYjXuaqOyKF9WS9StEQEZE77LXSE5ErI0ICdq2IC5MRL2tbd0SsvxTvY70RIiINqj4nfeR6cQAejHcpJiMakJVsxsrJwyT1+cM7u3C+WdzBS0RE5BlRPYIltZ8xOhHZKYG7VsSFyYhGZKfE4fHbB4luX9t4AUkLLMgvlL5QioiIlGepsCFPwuLV8UNi8dR4aSPj/orJiIb89vZBCA8xiG4vAHh5q5UJCRGRl1kqbJi5tgznmi6Iah/VIxh/nTRc5ah8B5MRDTHodXj45v6S+xV8YUXzBWm1SoiISBkOp4B57+6S1Cfz+j4Bv06kLSYjGjN7zCD0CJb2tjgF4J8lh9QJiIiIuuRwCnjinf/g7HlxIyIu4aFBKkXkm5iMaIxBr8PMWwZI7ne4ukGFaIiIqCuWChtGLSnGO2XSD8TrFx2uQkS+i8mIBs0eMwhR4dJWZPMbm4jIcywVNsxaWwZ7rfStvDodMDUjQfmgfBiTEQ0y6HV4dkKy6Pb8xiYi8hxXyXe5J3MMvcqIEIkHpPo7/mtoVK8I8Yfhjbm2N7+xiYg8RG7Jd5dRA3srGI1/4BNMo+w150W3HWw2qhgJERG1JeuA0zYyBlypUCT+g8t5Naq6vll0279+vh8HT9Vh7GAz+vQMQ3piNLeMERGpRM4Bpy69woNxY38mI5diMqJR0VeIn6YBgI93V+Hj3VUAALMxDHk5SQF9HDURkVrSE6MRHmJAg4wjOfLvGcI/FjvBaRqNMkXKz7ztNY2YtbaMh+kREamkxSG90OSczEH8I7ELTEY0Kj0xWnZC4lrhvWhjJRxOueu9iYioM9sPnEaLQ/rv1oSYCBWi8Q9MRjTKoNdh4V3yD1ASANhqGlFqrVYuKCIiwr++PiSrnztrTfwdkxENy0o2Y+XkNOjcmF50d9U3ERH9yOEU8O+9pyT3Mxsvbi6gzjEZ0bjsFDP+NnGY7P7MxImIlFNqrUa9xIWrOgB5OUlcuHoZTEZ8wJU9pe2scWEmTkSkrM++lbYxoFd4MFZNSePC1W4wGfEBcqdazrc4UFRpVzgaIqLA5HAKeKfsB9HtB8RE4JunxzIREYHJiA+QO9VS09DCLb5ERAoptVajrlH8FM396VdzakYkWcnIihUrkJCQgLCwMIwYMQKlpaWXbX/27Fk8+uijMJvNCA0NxTXXXIPCwkJZAQciudt8ucWXiEg5UkepY2ROsQciycnIhg0bMHfuXOTl5aGsrAypqakYN24cTpw40Wn75uZmjB07FocOHcI777yD77//HgUFBejbt6/bwQcKd7b5cosvEZEypI5Su1O8MtBITkZefPFFzJgxA9OnT0dSUhJWr16N8PBwrFmzptP2a9asQXV1NT744AOMGjUKCQkJuOWWW5Camup28IEkK9mM1VPSEBYsb2aNW3yJiNwzND5KdNvoiGBuIJBA0pOtubkZO3bsQGZm5o8X0OuRmZmJkpKSTvt89NFHyMjIwKOPPorY2FgkJyfj2WefhcPR9bxbU1MTamtr233QxYQk/2dDZPXlFl8iIvkKd9mQ/uwm0e2fuTuZ60UkkJSMnDp1Cg6HA7Gxse1ej42Nhd3e+a6NgwcP4p133oHD4UBhYSHmz5+PF154Ac8880yXXyc/Px9Go7H1Iz4+XkqYfs1k7CG5z5URIczQiYhkyi+sxCPrylDXeEFU+zHX9UZ2SpzKUfkX1XfTOJ1O9OnTB6+88gqGDx+OiRMn4qmnnsLq1au77JObm4uamprWj6NHj6odps9IT4xGdESwpD6p8UZm6EREMhTuOo6Xt1ol9ZkxeoBK0fivICmNY2JiYDAYUFVV1e71qqoqmEymTvuYzWYEBwfDYDC0vnb99dfDbrejubkZISEhHfqEhoYiNJSrkDtj0Ovws6F98eq2Q6L7/OdoDRxOgQkJEZEEDqeAP7y7S1IfFpuUR9LISEhICIYPH47i4uLW15xOJ4qLi5GRkdFpn1GjRmH//v1wOn88bnnv3r0wm82dJiLUvcykzhO/rpyub8ayou9RcuA0t/gSEYm0fPN+nGsSX1eEZd/lkzxNM3fuXBQUFOCNN97Anj17MGvWLNTX12P69OkAgGnTpiE3N7e1/axZs1BdXY3HHnsMe/fuxSeffIJnn30Wjz76qHJ3EWDSE6MR1UPaVM3yzw9gUsF23LR0M4ugERF1w+EUsObLg6LbR0ew7Ls7JE3TAMDEiRNx8uRJLFiwAHa7HUOHDoXFYmld1HrkyBHo9T/mOPHx8fj0008xZ84cpKSkoG/fvnjsscfwxBNPKHcXAcag12H6qEQs27RXcl97TSNmrS3jDw0R0WWUWqtRI3LBKgC89IthGH1tbxUj8m86QRA0P25fW1sLo9GImpoaREZGejscTXA4BQx/pghnG1ok99UBMBnD8OUTYzicSETUiQ/Lj+Gx9eWi2790/1DcPZTFPC8l9vnNs2l8lEGvw5J75NUcYVVWIqLL2yTxkFHWcnIPkxEflpVsxsrJwyB3cINVWYmIOircZcPGXeKTEVNkKHfQuInJiI/LTonD8klpsvoykycias/hFPD0hxWS+iy8azCnvN3EZMQPZKeY8eCoBEl9onoEMZMnImrD4RTw+jYrquubRfdZOXkYNwMogMmIn5Bae+Ts+Qt4zrJHpWiIiHyLpcKGm5ZuxuJPpP1e7BXBAp1KYDLiJ9ITo9EzTNpO7Ze3WvFx+TGVIiIi8g2WChtmrS2DrUb6OjquvVMGkxE/YdDrkD9B+u6a2evLUbiLRdCIKDA5nAIWbayE3BoXXHunDCYjfuSnQ+MwvF+U5H6PrCtjVVYiCkil1mpZIyIAT0RXEpMRP/PmjM7PCOrOoo2VPLeGiAKOO9Msi+9O5i4ahTAZ8TM7Dp+R1Y9F0IgoEMXIXIA6fdTVyE7hLhqlMBnxM+5k+VyIRUQBR+bAxh1JccrGEeCYjPgZdxZTcSEWEQWaU+eaZPXjH2/KYjLiZ9ITo2E2Sk8qzMYwLsQiooAj948w/vGmLCYjfsag1yEvJ0nyyOP88ddzIRYRBZz0xGiEGKT97uMfb8pjMuKHspLNWDUlDdERwaL7HDvbiA/Lj6HkwGnuqiGigPFphQ3NDmm/8/JykvjHm8Kklewkn5GVbMb5FifmbCgX1f5PhT+WQDYbw5CXk8TzFojIrzmcAua89R9Jfe5Lu4q/G1XAkRE/ZoqUN6dpr2nErLUshEZE/u2r/afQdMEpqc+oQTEqRRPYmIz4sfTEaJgipe+hdw1YshAaEfmzd3b8ILmP3D/y6PKYjPgxg16HhXcNltVXAAuhEZH/cTgFlBw4jQ/Lj2GPrVZSXy5cVQ/XjPi5rGQzVk9Jw7z3duNsQ4vk/txLT0T+wlJhw6KNlbLOotGBC1fVxJGRAJCVbMaOp8di+NVRkvtyLz0R+QNLhQ2z1pbJSkR6hgVh1ZQ0LlxVEZORAGHQ6zBusElSn7AgPYckicjnOZwCFm2shNwVcAt/yt2FamMyEkCuN0VKat94wYnnLHu6b0hEpGGl1mpZIyIuJmMPBaOhzjAZCSDV55sl9yn4wopmiVvfiIi0xO21b1wmojomIwFEzvoPpwD8s+SQ8sEQEXmIu2vf5B6mR+IxGQkgw/v1gpyF4IerG5QPhojIQ+QeIOrChfzqYzISQHYcPgM5Ncz6RYcrHwwRkYcY9DrclSpvAWp0RDAX8nsAk5EAInfedGpGgrKBEBF5kKXChle2WmX1febuZNYW8QAmIwFE7lDjC599p3AkRESe4c623odvTkR2SpziMVFHTEYCiNx5U+6oISJfJWdb75URIVg5OQ252UkqRUWXYjISQAx6HfJypP9wcUcNEfkqqdPT88dfj9KnMpGdwiJnnsRkJMC4zqqJ6iHtWCLr6XqVIiIiUo/U6WmzMYxrRLyAyUgAyko2Y8f8O/BTCZk/fzSJyNc4nAKcTgFBEpKLxZ/sgUPOtkNyC5ORAGXQ67B8choeuLGfqPaCAJQcOM0fUiLyCZYKG25auhm/fPVrXJDwe8tW04hSa7WKkVFnmIwEuKwh4kZH1n59BJMKtuOGP21C4S6bylEREclXuOs4Zso8oRdQoHw8ScZkJMBJ3WFTXd+MR9aVIb+wUsWoiIjkKdxlw+w3d7p1DVZc9TwmIwFO7g6bl7daUbjruAoRERHJY6mw4ZF1ZbIqTQMX18aZjWGsuOoFTEYIWclm3JfWV3K//33nP1xDQkSa4HAKWPiR+yO2eTlJ3E3jBUxGCAAQHiptqy8A1Dc7sf3gaRWiISKSZvnmfbDXyl/rYTaGYdWUNGQls76IN0h/ApFfknsYXsmB0xg1MEbhaIiIxLNU2LBs0z5ZfZPjIvHU+CSkJ0ZzRMSLODJCAC4ehifv55DTNETkPa6zZ+S4K8WEj/9nNDIGXMlExMuYjBAAICRIjxmjEyX3y+jPUREi8h45Z8+4DO4bpWwwJBunaaiV61CoV7ZaRY93nKlvVi8gIqJuFFXaZfc9dva8gpGQOzgyQu3kZidh98JxotvPe38XT/QlIq9wOAW8v/OY7P5y18qR8piMUAdXhAVhaLxRVNtzTQ6kLS6CpYJVWYnIs5Zv3o8zDS2y+upwca0caQOTEepUwpURotuea7qAmWvLmJAQkcdc3EGzV3b/h0YnICSIj0Ct4DtBnYqLkl4OOfe93SyCRkSqc2cHDQBkXt8bT40frGBE5C4mI9SpUQN6S+5zpqEFyzfL2+tPRCSWOztospNN+PsD6QpHRO5iMkKdunHAlYgKD5bc75WtBzk6QkSqcudU3XHJJgUjIaUwGaFOGfQ6LLlniOR+9c0OLCv6XoWIiIguOnSqXnZfnsirTUxGqEtZyWbcPEh6UbPlnx/AjH/8PxUiIqJA507p96jwYJ7Iq1FMRuiy+l0pbx9+UeUJ/OkT90/QJCJyuXgy77ey+08fmciy7xrFZIQuS8oW30sVfGFlQTQiUkyptRr22iZZfaPCgzF7zECFIyKlMBmhy5J/gN5Fue/tUi4YIgpom9wo/b7kniEcFdEwWcnIihUrkJCQgLCwMIwYMQKlpaWi+q1fvx46nQ4TJkyQ82XJC+QeoOfyXtkxFO46rmBERBSImi848daOo5L7mY1hWD0lDVnJZhWiIqVITkY2bNiAuXPnIi8vD2VlZUhNTcW4ceNw4sSJy/Y7dOgQfv/732P06NGygyXvyM1OwsM3J8oaIREAPLJuJ6uzEpFslgobbswvRl2jQ1K/OZmD8OUTY5iI+ADJyciLL76IGTNmYPr06UhKSsLq1asRHh6ONWvWdNnH4XDgl7/8JRYtWoT+/fu7FTB5R252Er5bfKes3TUAsGhjJeuPEJFklgobZq4tQ7XEE8IfHJWAxzKv4dSMj5CUjDQ3N2PHjh3IzMz88QJ6PTIzM1FSUtJlvz/+8Y/o06cPHnzwQVFfp6mpCbW1te0+yPtCgvS4d/hVsvraahpRaq1WOCIi8mcOp4An3pW37iwzicXNfImkZOTUqVNwOByIjY1t93psbCzs9s4XFn355Zd49dVXUVBQIPrr5Ofnw2g0tn7Ex8dLCZNU5E7BIHeqJhJR4Plb8T7UnL8gud8VoUGsJ+JjVN1NU1dXh6lTp6KgoAAxMeKH93Nzc1FTU9P6cfSo9EVLpI70xGiYjfISEneqJhJRYLFU2PCXYnnFzQycmfE5kpKRmJgYGAwGVFVVtXu9qqoKJlPHIbEDBw7g0KFDyMnJQVBQEIKCgvCPf/wDH330EYKCgnDgwIFOv05oaCgiIyPbfZA2GPQ65OUkyer72jYr140QUbfcPZW3pvECp4V9jKRkJCQkBMOHD0dxcXHra06nE8XFxcjIyOjQ/rrrrsPu3btRXl7e+nHXXXfhtttuQ3l5OadffFRWshkrJ6dJ3l1z9vwFbD94Wp2giMhvuHMqrwunhX1LkNQOc+fOxQMPPICf/OQnSE9Px1/+8hfU19dj+vTpAIBp06ahb9++yM/PR1hYGJKTk9v1j4qKAoAOr5NvyU4xYzmG4ZF1OyX1e/K9XVhybyrSE6O5yp2IOqVEIsED8XyL5GRk4sSJOHnyJBYsWAC73Y6hQ4fCYrG0Lmo9cuQI9HoWdg0E2SlxuHdPFd7dKb6o2eHq85hUsB1mYxjycpK4/5+IOoi5ItSt/qbIUC5g9TE6QRA0P4lfW1sLo9GImpoarh/RmC/2nsTUNeIq8F5KB2AVKyMS0SWWFX2Pl4r3y+7PiqvaIfb5zSEMcsspiYWI2hLAYmhE1J6lwiY7EYkINTAR8VGSp2mI2qo+J+8ETRdXMbSMAVcqFBER+Sp3dtFER4Rge+7tCAni39i+iMkIuSU6IsTta3DVOxEBwFf7TkneReNaBv/sz5KZiPgwJiPkFpOxh9vX4Kp3IsovrMQrW62S+5m4GN4vMBkht7gqssqtCRAdHsxV70QBLr+wEi/LSETmj78evxqVyDIBfoBjWuQWV0VWub8KGi84UFTZ+blGROT/mi84UfCF9ESkZ6iBiYgfYTJCbstKNmPVlDSYIqXXBmhodmLm2jJYKmwqREZEWvfPkkOQs6Guf+8rmIj4ESYjpIisZDO2zbsdczKvkdV/3nu7ucWXKAAdrm6Q1a9/TITCkZA3MRkhxRj0OjyWOQgrJ6dJ7nu2oQXbD/DcGqJAE99L3iL4e9KuUjgS8iYmI6S4cckmRIRI/9YqOXhKhWiISMuuM0mvqh0RasDIgTEqREPewt00pLhSazXqm52S+x04Wa9CNESkRQ6ngO0HTuP5z76T3Pc3owdwvYifYTJCipNbxOz/KuywVNhYL4DIz1kqbJj33m6cbWiR1T8hJlzhiMjbOE1DinOniBnPqiHyb5YKG2auLZOdiAAslOiPmIyQ4lyF0OQMotpqGrF88z7FYyIi73M4BSz86Fu3rmE2hrFQoh9iMkKKcxVCAyArIVm2aR/rjhD5oVJrNey18g/X1AHIy0niehE/xGSEVNFaCM0obzh14UffcrqGyM985ka15V7hwVg1JY1ryvwUkxFSTVayGV8+MQaP3jZAcl97bRNKrdUqREVE3mCpsOG1bYck99MBeOz2gfjm6bFMRPwYkxFSlUGvw00De8vq++W+kxwdIfIDDqeAee/tltXXGB6M/7n9Gk7N+DkmI6S69MRoREeESO63YssB3LR0M9ePEPm47QdOy949c7ahhaOkAYDJCKnOoNfhmbuTZfW11TRi5toyvLRpL0dJiHyUu9WV5dYuIt/BZIQ8IjvFjBmjE2X3X7ZpH0Yt4SgJkS9xOAWUHDiNjf9x7+eWdUX8Hyuwksc8NT4JgICCLw7J6m+vbcSstWVcUU/kAywVNizaWAlbjXujGtERwawrEgA4MkIe9dT4wVg5OQ1hQfK+9QQAT76/G80XpJ99Q0SeYamwYdbaMrcTEQB45u5kLl4NAExGyOOyU8z49o9ZGJckb5dNdX0Lbswv5pQNkQY5nAIWbayEEiu8Hr45EdkpcQpcibSOyQh5hUGvQ3qi/CPAq+ubMWttGRMSIo0ptVa7PSKiA7D8/mHIzU5SJijSPCYj5DXRV4S6fQ0erEekLUrsfFkxOQ0/HcoRkUDCZIS8xhTp3gp5ARe3/rIGAZF2uLvz5dejEpCdwgXqgYbJCHmN63Rfd7EGAZF2pCdGwxQpf9RzbJJJwWjIVzAZIa9pe7qvO1iDgEg7iirtaJS5243beAMXkxHyqqxkM1ZPSUNEiEH2Nc7Uyz+SnIiUU7jLhplry2SXfuc23sDFZIS8LivZjJ0L7kCoQd4voT9+zEWsRN5WuOs4Zr9ZJrs/t/EGNiYjpAkhQXo8cttAWX3ttU1Yvnm/whERkViWChseWbcTcv8mmD4qgdt4AxzLwZNmJMREyO67bNPe/14jHH16hiE9MZrDvUQe4Cpy5o47uGg14DEZIc1wdyGqKyEBALMxDHk5STzDhkhl7hY5MxvDuGiVOE1D2pGeGI3oiBBFrmWraWSFViIPcHdrfV5OEkcxickIaYdBr8MEBasuCgBy39vNxa1EKnE4BZyqk7+bbU7mII5eEgAmI6QxShc8OtPQguWb9yl6TaJA53AKeGnTXgxfXITFn+yRdQ2zMQyzxwxSODLyVUxGSFPSE6PRKzxY0Wu+tu0QR0eIFGKpsGH4M0VYtmkfzp6XV09EB07PUHtMRkhTDHod/jQhWdFrnj3fwvNriBRgqbBhlhtFzQCgV3gwVk1J4/QMtcNkhDQnOyUOY67rreg1eX4NkXtcW3jdGWPMSTHhm6fHMhGhDpiMkCbNGD1A0evx/Boi97izhTc6IhgrJw/D3yYP59QMdYp1RkiTXCf6ulO/ALg4N21iHQMit8kdXZx649VYeBfPnKHL48gIaZLrRF93f30JAO6/IR4f7zqOkgOnuZCVSCa5o4tD43sxEaFucWSENCsr2YxVU9Kw8KNvYa+VXstApwN6BBuwbNOPW3tZmZVInvTEaBh7BKNG4g6asw3NKkVE/oQjI6RpWclmbJt3O+ZkXiO5ryAADc2Odq/ZWZmVSJaiSjsami5I7qdUVWXyb0xGSPMMeh0eyxyElZPTFJm2AYBFGys5ZUMkkqXChplry9Ai42fGZOyhQkTkb5iMkM/ITjFjWsbVbl9HwMWza1h7hKh7DqeAuRvKZfXlIXgkFpMR8ilZycqdXcPaI0SX53AKmPRKCRpanLL6s8oqicUFrORTlNryC7D2CNHlWCps+J/15Wi+ID0RiQgx4IVfpHKhOInGkRHyKa4tv+7S64Dh/XopEBGR/3GtEZGTiADA6inDmYiQJExGyOdkJZtl7a5pyykAOw6fUSgiIv/hcApY+NG3bl2jmtt5SSImI+STZo8ZCFNkqFvX+PsXBxSKhsh/lFqrZdX1aYtToCQVkxHySQa9DgvvGuzWVt/i706icBfrjRC15e7C7isjQriDhiRjMkI+y1Wh1WyU/1fY796Wt0CPyJ84nAJKDpzGh+XH8MXek25da/HdPIeGpJOVjKxYsQIJCQkICwvDiBEjUFpa2mXbgoICjB49Gr169UKvXr2QmZl52fZEUmQlm/HlE2Mw+zZ5p/yeb3EibfFnWLzxW55dQwHJUmHDTUs3Y1LBdjy2vhzvlB2Tfa2Hb05EdgoXrpJ0kpORDRs2YO7cucjLy0NZWRlSU1Mxbtw4nDhxotP2W7ZswaRJk/D555+jpKQE8fHxuOOOO3DsmPxveKK2DHodRg3sLbv/uSYHXt12CJMKtuOmpZtZKp4ChqXChllry9zeKh8dEYyVk4chN9v9nW4UmHSCIEj6U3DEiBG44YYbsHz5cgCA0+lEfHw8fvvb32LevHnd9nc4HOjVqxeWL1+OadOmifqatbW1MBqNqKmpQWRkpJRwKUA4nAJuWrpZkfojOgCrpqRxayL5NaV+ZnQAKv+YhR4hBmUCI78i9vktaWSkubkZO3bsQGZm5o8X0OuRmZmJkpISUddoaGhAS0sLoqO5wImUo1T9EeBiufh57+7ilA35LYdTwOvbrIok7wKAdV8fdj8oCmiSkpFTp07B4XAgNja23euxsbGw2+2irvHEE08gLi6uXUJzqaamJtTW1rb7IOpOVrIZKycPg06BtXNnz1/AY+t3un8hIo1xrRFZ/Mkexa55uLpBsWtRYPLobpolS5Zg/fr1eP/99xEW1vUOiPz8fBiNxtaP+Ph4D0ZJviw7JQ4rJqUpcq2Pd9m49Zf8ilJrRC7VLzpc0etR4JGUjMTExMBgMKCqqqrd61VVVTCZTJft+/zzz2PJkiX47LPPkJKSctm2ubm5qKmpaf04evSolDApwGWnmLF6ShqiwoPdvtb8Dys4XUN+wVVZVenvZr0OmJqRoPBVKdBISkZCQkIwfPhwFBcXt77mdDpRXFyMjIyMLvs999xzWLx4MSwWC37yk590+3VCQ0MRGRnZ7oNIiqxkM0qfzEREqHuL6k7XN6PUWq1QVETes3zzfrcrq3ZmxuhEhASxZBW5R/J30Ny5c1FQUIA33ngDe/bswaxZs1BfX4/p06cDAKZNm4bc3NzW9kuXLsX8+fOxZs0aJCQkwG63w26349y5c8rdBVEndhw+g/omh9vX2VQpbj0UkVYV7rJh2aa9il5Tr7tYV4TbeUkJQVI7TJw4ESdPnsSCBQtgt9sxdOhQWCyW1kWtR44cgV7/Y46zatUqNDc347777mt3nby8PCxcuNC96Ikuw92y1i6vbjuEGxKjudWXfFLhruOY/aYyi7HvS7sK4aEG9IsOx9SMBI6IkGIk1xnxBtYZITlKDpzGpILtilzLbAzDl0+MYZlr8imWChtmri1z+zpmYxjycpKYkJNkYp/fkkdGiHxFemI0zMYw2Gsa3V60Z6tpRKm1GhkDrlQkNiK1XVywWunWNcJDDCiY9hPc2P9KJuKkKo6xkd9qWwhNiV+jSk37EHnC8s37YK9173v2xV+kYtTAGCYipDomI+TXXCf7mtw42delqLKq+0ZEGmCpsGHZpn2y+4cG6bGaRyKQB3GahvxeVrIZY5NMKLVW40RdI76z12LVloOSr/PxLhvGDT6OnNQ4FaIkco/DKaDUWg17zXn88WP3pmceGp3IRIQ8iskIBQSDXte63iNmX6isZAQA/ufNndh/4hz+5/ZBHLomzbBU2LBoY6VilVVH9o9R5DpEYnGahgKPGzmEAOCl4n1IWWhB4a7jioVEJJfSJd6D9TrcyIXa5GFMRijgnDrnfhXK+mYnHlm3E/mF7g2HE7nD4RSwaGOloiXe7xoax1E/8jhO01DA6dPT/cWsLi9vtaL5goA7BpuQnhjNX+LkUaXWasUPvcu/5/JnhxGpgSMjFHBc9UeUShte++oQJhVsx01LN8NSwVN+yXM++1bZ7zeeM0Pewu86CjhK1x9xsdc0YtbaMhTusqHkwGl8WH4MJQdO89RfUkV+YSVe++qwYtcbm9QHT43nOTPkHSwHTwFL6R0ILnod0Db/YCltUlrhLhseWed+mXcACA/W47l7U/HTodyyTsoT+/xmMkIBzVWb4URdI2IiQvHbN3eiuqFZ0a/hGn1ZxSJSpIDmC04Mf6YIdY0XFLneI7cOwB+yrlPkWkSXEvv85jQNBTRX/ZG7h/bFqEExmDBM+b8OXdn+oo2VnLIht1gqbLgxf5NiiQhwcSSPyNuYjBC1MTbJpMp1Bfx42B6RHK56ItX1LYpeN4MFzkgDmIwQtZGeGI3oiBDVrs/D9kgONeqJAEBUeDALnJEmMBkhasOg1+GZu5NVu76SNU4ocKhRTwQAltwzhLVxSBOYjBBdIjvFjJwU5adreoUHIz0xWvHrkv/7obpe0euZIkN5Ki9pCiuwEnXiL/enYfP3n6K+yaHYNc80tKCo0s4HAInmcApYvnkf/rp5nyLXm33bAIwa2JvVgklzmIwQdcKg1+HP96bgkXU7Fb3uwo++xdgkEx8E1CXXdvOiSjve+uYHnGtSZufMFaEGzBl7Lb/3SJOYjBB1ITslDjOOnkXBF1bFrmmvbcLyzfvxWOYgxa5J/kOtQnwAMGpgDBMR0iyuGSG6jKfGJ+GhUYmKXnPZpr08w4Y6cG3dVSMRAYBBfXqqcl0iJXBkhKgbT+ckQacXUPDFIcWu+bu3ynG+xQlTZBjn70m1rbttZXALL2kYkxEiEZ4aPxjD4nth9rqdcCpwvfpmJ+ZsKAfAs2tIva27Lr3Cg3FjfyYjpF2cpiESKTslDvuezUZ2srLbfl2n/XLqJjA5nAK27j2h6tfIZz0R0jgmI0QSGPQ6rJwyHA+M7KfYNYX/fvz+7f/gi+9P8vyaAGKpsOGmpZux6t8HVbm+2RjGeiLkEzhNQyRD1mAz3vjqsKLXPNfkwNTXShEVHowl9wzB2CRT64nCfXpybYm/cS1YVTL1vDetL+4ZdhVO1Tfxe4Z8CpMRIhnSE6NhNoapMs9/tqEFM9eWISo8GGcbfjwUjWtL/IPDKWD7wdOY9+5uxRes3nxNb4waxIPvyPdwmoZIBoNeh7ycJKj5N2fbRATg2hJf53AKeGnTPgxfXIRf/v1rnD2v7Om7AM8+It/FZIRIpqxkM1ZNSUN0RLBHvp7rr+hFGyu5rsTHFO46jtRFn2LZpr2qJCE6XBw549lH5KuYjBC5ISvZjPk/HeyxrycAsNU0otRa7bGvSe750yff4pF1O3FOwXOO2nKNzuXlJHF9CPksJiNEbjJFen5o/ESdejUpSDl/+qRS0WJ5ADpMDZqMYVjFHTPk47iAlchNrsWs9ppGVStotnXolLJHypMyXIfcnahrhPVkvaLnGgHA/PHXY2pGAnYcPsNdVuRXmIwQucm1mHXW2jKPfc1lm/YB0CEhJpwPJI1Q85A7AIjqEYxfjUqEQa9jaXfyO0xGiBTgWsy68KNK2Gs9M4WybNPe1v/Pbb/epUbNkEtNH5XAhJP8FteMECkkK9mMbfPGYE7mII9/bW779Q6HU8C2/adUqRnSVq/wYMwe4/nvKyJP4cgIkYIMeh0ey7wG15p6qjpkfykBFxc2LtpYibFJJv4F7QFqT8u0xbNlyN8xGSFSQVayuV059+jwEMxc+w3qm5U487dzrm2/r2+ztq4tIHV4YloGAPQ6YPkk7pQh/6cTBEHz1ZNqa2thNBpRU1ODyMhIb4dDJIulwoaZHlrkyjUk6nE4Bdy0dLNHRkRWTh6G7JQ41b8OkVrEPr+5ZoTIQ7KSzR5bT2KracTMtWV4adNeVmtVWKm12iOJyJzMa5iIUMBgMkLkQbPHDPJokbRlm/YhbXERXtq0j0mJQjZV2lX/GqbIUMweM1D1r0OkFUxGiDzIoNdh4V3qHrB3qZrzLVi2aS+GP1PE3TaX4XAKKDlwGh+WH8O2faewbf8pfFh+DCUHTrcmcpYKG17ddkj1WBbeNZhrfiigcM0IkRdYKmwerUnS1uopaRibZML2A6dRcvAUgItFtG7sf2XAPgC72xlj7BGE6SMT8NpXh1Bz/oJqcUSFB2PJPUO41of8htjnN5MRIi9xOAUs37zvv9VUPSc8WI+QID3OXvJQvSI0CM/dm4LslMB6EHpqZ0x37kvri6X3pQZsQkj+iQtYiTTOVZNk9ZQ0mI2eW0fS0OLskIgAwLmmC3hkXRnyCys9Fou3nW924H/f+Y/XExFTZCgTEQpoTEaIvCwr2YwvnxiDp7Kv93YoAICXt1pRuOu4t8NQRNt1IG3XfgAXT9RNWmBBXaPDa/Hp/vvBNSIU6Fj0jEgDDHod+kSGejuMVk9/WIFxyWaffkBeXJfzLey1Ta2vmSJDsfCuwXi37AcUVZ7wYnT/jYf1YIgAMBkh0ow+PT03VdOd6voWlFqrffZ02K4KzNlrmzxWeO5yonoEY8Uv0wJ60TBRW0xGiDQiPTEaZmMY7DWNXl/DAAAn6n7cWeJwCq2l7fv0DEN6YrRmH6IOp4B57+32dhiXteTeIRg1MMbbYRBpBpMRIo0w6HXIy0nCLA385Q4A3xyqxp3JZmz+rqrDtld3ys13ltgAaPfa8H69sOPwmQ7Jj5ikaPuB0zjb0OLezavk4lkzwzgtQ3QJbu0l0hhPngYrlw4XD+abkzkICTERHRIDV9JgrzmP6vpmRF8RClNkGM7UN2HxJ3va3VtUeDAAtEsg9DqgbcFYszEMd6Wa8dF/bN0mRc9/+h2Wf35AnRt308rJaQG3dZoCG+uMEPkw18O8qNKOD8qPo7q+ufVzUeHBmvzL3xQZhoV3JQGAx5OplZOHoVdEKOy1jfiz5Tsc11gix4MLKVAxGSHyE51NTRRV2vHk+xXtkpRA5hqp0ZJ//jod1Q3Nml9jQ6Qmsc9vrhkh0jiDXtdhV0tWshljrovFjfmbUF2vvVEST9NaIvLwzYkYfU1vb4dB5DNY9IzIR4UE6fHsz4a0Fs4i79PrLiYiudlJ3g6FyKdwZITIh2Ulm7FqSprmF7z6s55hBkwYehUSrgzH1IwEhATxbzwiqWT91KxYsQIJCQkICwvDiBEjUFpaetn2b7/9Nq677jqEhYVhyJAhKCwslBUsEXXkKif/5owb8dL9Q/GvB0fg7pQ4b4cVEHQA/nxfKhZPSMaDo/szESGSSfJPzoYNGzB37lzk5eWhrKwMqampGDduHE6c6Ly08ldffYVJkybhwQcfxM6dOzFhwgRMmDABFRUVbgdPRBe51pXcPbQvRg2KwS/S470dkt8zG8Owakoad8gQKUDybpoRI0bghhtuwPLlywEATqcT8fHx+O1vf4t58+Z1aD9x4kTU19fj448/bn3txhtvxNChQ7F69WpRX5O7aYikcTgFDH+mSJNbgH3FfWl9MWpgzMUy/TrgRF0Tqs81IToiBCZjD+6QIRJBld00zc3N2LFjB3Jzc1tf0+v1yMzMRElJSad9SkpKMHfu3HavjRs3Dh988EGXX6epqQlNTT8eblVbWyslTKKAZ9DrsOSeIZo4h8XXsCYIkedJmqY5deoUHA4HYmNj270eGxsLu93eaR+73S6pPQDk5+fDaDS2fsTHc8iZSKqsZDNWT0mDKVI7B/Bp3ZzMQfjyiTFMRIg8TJOrrXJzc1FTU9P6cfToUW+HROSTspLN2Dbv4uLW2bcN8HY4mjYn8xo8lnkNp16IvEDSNE1MTAwMBgOqqqravV5VVQWTydRpH5PJJKk9AISGhiI0NFRKaETUBdfi1vTEaLxbdkwzpwJrTUJMuLdDIApYkkZGQkJCMHz4cBQXF7e+5nQ6UVxcjIyMjE77ZGRktGsPAEVFRV22JyJ1uE4FBlgkrTN9enI6i8hbJE/TzJ07FwUFBXjjjTewZ88ezJo1C/X19Zg+fToAYNq0ae0WuD722GOwWCx44YUX8N1332HhwoX45ptvMHv2bOXugohEcRVJMxn9/8HbM0z8wK/ZePH8GCLyDskVWCdOnIiTJ09iwYIFsNvtGDp0KCwWS+si1SNHjkCv/zHHGTlyJNatW4enn34aTz75JAYNGoQPPvgAycnJyt0FEYmWlWzG2CQTSq3VsNc2YvHH3/rd+TZzMgdh1q0DccufPxc1LZWXk8S1IkRexFN7iQKcpcKGWf/dAqz5XwZtJJl7wlbTiDNtaqmYIkOx8K7Brbthuru3qPBgLLlnCHfPEKlE7PObyQgRwVJhu+z5NqbIUNx/QzyaHQL+UXII55ocnbbTATAZwzB//PVY/MmedtcLC9ajscWpWMzzx1+PX41KRKm1GifqGtGnZ1inhcg6u7eo8GBMH5mI2WMGckSESEVMRohIEodTaH2wx1wRCgjAqfqmDg/5rkYbXI90V4n0ttdzXaOo0o6FH1XCXtt50qP77zV1OuByv5n0OuC7xXeKPgums1iYhBCpj8kIEamms9EGsZVLXYlBUaUdH5QfR3V9c4dr7DxyBi9vtXZ5jYdvTkRudpL7N0JEqmIyQkSqUmK04XLXyC+sRMEXVjjb/IbS64AZo5mIEPkKJiNE5POaLzjxz5JDOFzdgH7R4ZiakSB6aoaIvE+Vg/KIiDwpJEiPB0f393YYRKQy/olBREREXsVkhIiIiLyKyQgRERF5FZMRIiIi8iomI0RERORVTEaIiIjIq5iMEBERkVcxGSEiIiKvYjJCREREXuUTFVhdFetra2u9HAkRERGJ5Xpud3fyjE8kI3V1dQCA+Ph4L0dCREREUtXV1cFoNHb5eZ84KM/pdOL48ePo2bMndDppp4JeTm1tLeLj43H06FG/PIDP3+8P8P979Pf7A/z/Hnl/vs/f71HN+xMEAXV1dYiLi4Ne3/XKEJ8YGdHr9bjqqqtUu35kZKRffoO5+Pv9Af5/j/5+f4D/3yPvz/f5+z2qdX+XGxFx4QJWIiIi8iomI0RERORVAZ2MhIaGIi8vD6Ghod4ORRX+fn+A/9+jv98f4P/3yPvzff5+j1q4P59YwEpERET+K6BHRoiIiMj7mIwQERGRVzEZISIiIq9iMkJERERe5XfJyIoVK5CQkICwsDCMGDECpaWll23/9ttv47rrrkNYWBiGDBmCwsLCdp8XBAELFiyA2WxGjx49kJmZiX379ql5C5cl5f4KCgowevRo9OrVC7169UJmZmaH9r/61a+g0+nafWRlZal9G12Scn+vv/56h9jDwsLatdHa+wdIu8dbb721wz3qdDqMHz++tY2W3sOtW7ciJycHcXFx0Ol0+OCDD7rts2XLFqSlpSE0NBQDBw7E66+/3qGN1J9rtUi9v/feew9jx45F7969ERkZiYyMDHz66aft2ixcuLDD+3fdddepeBeXJ/Uet2zZ0un3qN1ub9fOV9/Dzn6+dDodBg8e3NpGS+9hfn4+brjhBvTs2RN9+vTBhAkT8P3333fbz9vPQr9KRjZs2IC5c+ciLy8PZWVlSE1Nxbhx43DixIlO23/11VeYNGkSHnzwQezcuRMTJkzAhAkTUFFR0drmueeew1//+lesXr0aX3/9NSIiIjBu3Dg0NjZ66rZaSb2/LVu2YNKkSfj8889RUlKC+Ph43HHHHTh27Fi7dllZWbDZbK0fb775pidupwOp9wdcrBjYNvbDhw+3+7yW3j9A+j2+99577e6voqICBoMBP//5z9u108p7WF9fj9TUVKxYsUJUe6vVivHjx+O2225DeXk5Hn/8cTz00EPtHthyvi/UIvX+tm7dirFjx6KwsBA7duzAbbfdhpycHOzcubNdu8GDB7d7/7788ks1whdF6j26fP/99+3uoU+fPq2f8+X38KWXXmp3X0ePHkV0dHSHn0GtvIf//ve/8eijj2L79u0oKipCS0sL7rjjDtTX13fZRxPPQsGPpKenC48++mjrfzscDiEuLk7Iz8/vtP0vfvELYfz48e1eGzFihPDwww8LgiAITqdTMJlMwp///OfWz589e1YIDQ0V3nzzTRXu4PKk3t+lLly4IPTs2VN44403Wl974IEHhLvvvlvpUGWRen+vvfaaYDQau7ye1t4/QXD/PVy2bJnQs2dP4dy5c62vaek9bAuA8P7771+2zR/+8Adh8ODB7V6bOHGiMG7cuNb/dvffTC1i7q8zSUlJwqJFi1r/Oy8vT0hNTVUuMAWJucfPP/9cACCcOXOmyzb+9B6+//77gk6nEw4dOtT6mpbfwxMnTggAhH//+99dttHCs9BvRkaam5uxY8cOZGZmtr6m1+uRmZmJkpKSTvuUlJS0aw8A48aNa21vtVpht9vbtTEajRgxYkSX11SLnPu7VENDA1paWhAdHd3u9S1btqBPnz649tprMWvWLJw+fVrR2MWQe3/nzp1Dv379EB8fj7vvvhvffvtt6+e09P4ByryHr776Ku6//35ERES0e10L76Ec3f0MKvFvpiVOpxN1dXUdfgb37duHuLg49O/fH7/85S9x5MgRL0Uo39ChQ2E2mzF27Fhs27at9XV/ew9fffVVZGZmol+/fu1e1+p7WFNTAwAdvufa0sKz0G+SkVOnTsHhcCA2Nrbd67GxsR3mLl3sdvtl27v+V8o11SLn/i71xBNPIC4urt03VFZWFv7xj3+guLgYS5cuxb///W/ceeedcDgcisbfHTn3d+2112LNmjX48MMPsXbtWjidTowcORI//PADAG29f4D772FpaSkqKirw0EMPtXtdK++hHF39DNbW1uL8+fOKfN9ryfPPP49z587hF7/4RetrI0aMwOuvvw6LxYJVq1bBarVi9OjRqKur82Kk4pnNZqxevRrvvvsu3n33XcTHx+PWW29FWVkZAGV+d2nF8ePH8X//938dfga1+h46nU48/vjjGDVqFJKTk7tsp4VnoU+c2kvuW7JkCdavX48tW7a0W+R5//33t/7/IUOGICUlBQMGDMCWLVtw++23eyNU0TIyMpCRkdH63yNHjsT111+Pl19+GYsXL/ZiZOp49dVXMWTIEKSnp7d73Zffw0Cybt06LFq0CB9++GG79RR33nln6/9PSUnBiBEj0K9fP7z11lt48MEHvRGqJNdeey2uvfba1v8eOXIkDhw4gGXLluGf//ynFyNT3htvvIGoqChMmDCh3etafQ8fffRRVFRUeHUNklh+MzISExMDg8GAqqqqdq9XVVXBZDJ12sdkMl22vet/pVxTLXLuz+X555/HkiVL8NlnnyElJeWybfv374+YmBjs37/f7ZilcOf+XIKDgzFs2LDW2LX0/gHu3WN9fT3Wr18v6hebt95DObr6GYyMjESPHj0U+b7QgvXr1+Ohhx7CW2+91WE4/FJRUVG45pprfOL960p6enpr/P7yHgqCgDVr1mDq1KkICQm5bFstvIezZ8/Gxx9/jM8//xxXXXXVZdtq4VnoN8lISEgIhg8fjuLi4tbXnE4niouL2/313FZGRka79gBQVFTU2j4xMREmk6ldm9raWnz99dddXlMtcu4PuLgCevHixbBYLPjJT37S7df54YcfcPr0aZjNZkXiFkvu/bXlcDiwe/fu1ti19P4B7t3j22+/jaamJkyZMqXbr+Ot91CO7n4Glfi+8LY333wT06dPx5tvvtluS3ZXzp07hwMHDvjE+9eV8vLy1vj94T0ELu5S2b9/v6g/CLz5HgqCgNmzZ+P999/H5s2bkZiY2G0fTTwLFVkGqxHr168XQkNDhddff12orKwUfvOb3whRUVGC3W4XBEEQpk6dKsybN6+1/bZt24SgoCDh+eefF/bs2SPk5eUJwcHBwu7du1vbLFmyRIiKihI+/PBDYdeuXcLdd98tJCYmCufPn9f8/S1ZskQICQkR3nnnHcFms7V+1NXVCYIgCHV1dcLvf/97oaSkRLBarcKmTZuEtLQ0YdCgQUJjY6Pm72/RokXCp59+Khw4cEDYsWOHcP/99wthYWHCt99+29pGS++fIEi/R5ebbrpJmDhxYofXtfYe1tXVCTt37hR27twpABBefPFFYefOncLhw4cFQRCEefPmCVOnTm1tf/DgQSE8PFz43//9X2HPnj3CihUrBIPBIFgsltY23f2bafn+/vWvfwlBQUHCihUr2v0Mnj17trXN7373O2HLli2C1WoVtm3bJmRmZgoxMTHCiRMnPH5/giD9HpctWyZ88MEHwr59+4Tdu3cLjz32mKDX64VNmza1tvHl99BlypQpwogRIzq9ppbew1mzZglGo1HYsmVLu++5hoaG1jZafBb6VTIiCILwt7/9Tbj66quFkJAQIT09Xdi+fXvr52655RbhgQceaNf+rbfeEq655hohJCREGDx4sPDJJ5+0+7zT6RTmz58vxMbGCqGhocLtt98ufP/99564lU5Jub9+/foJADp85OXlCYIgCA0NDcIdd9wh9O7dWwgODhb69esnzJgxwyu/IFyk3N/jjz/e2jY2NlbIzs4WysrK2l1Pa++fIEj/Hv3uu+8EAMJnn33W4Vpaew9d2zwv/XDd0wMPPCDccsstHfoMHTpUCAkJEfr37y+89tprHa57uX8zT5J6f7fccstl2wvCxa3MZrNZCAkJEfr27StMnDhR2L9/v2dvrA2p97h06VJhwIABQlhYmBAdHS3ceuutwubNmztc11ffQ0G4uI21R48ewiuvvNLpNbX0HnZ2bwDa/Vxp8Vmo+2/wRERERF7hN2tGiIiIyDcxGSEiIiKvYjJCREREXsVkhIiIiLyKyQgRERF5FZMRIiIi8iomI0RERORVTEaIiIjIq5iMEBERkVcxGSEiIiKvYjJCREREXsVkhIiIiLzq/wMcHnDQoJGSRwAAAABJRU5ErkJggg==",
      "text/plain": [
       "<Figure size 640x480 with 1 Axes>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "\n",
    "\n",
    "N = 1000\n",
    "noise_level = 1e-2\n",
    "# define the true functional relationship between x and y\n",
    "truefunction = lambda x : (x-1)**2\n",
    "\n",
    "# generate synthetic data, random samples on [0, 2]\n",
    "xdata = 2*np.random.rand(N)\n",
    "# generate noise to add to the true function\n",
    "noise = np.random.randn(N)*noise_level\n",
    "ydata = truefunction(xdata) + noise\n",
    "\n",
    "plt.scatter(xdata, ydata)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.6"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
