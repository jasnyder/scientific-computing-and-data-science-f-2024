# Scientific Computing and Data Science F2023

This is a hub for the course materials associated with Scientific Computing and Data Science, taught at RUC in the spring semester 2024.

This course will primarily use Python, and the exercises here will be hosted in Jupyter notebooks (`*.ipynb` files). To use them, you will need:
* A program that can run Jupyter notebooks such as [Jupyter Lab](https://jupyter.org/install), [Spyder](https://docs.spyder-ide.org/current/installation.html), or [VSCode](https://code.visualstudio.com/)
* A working version of Python with at least the following packages installed:
    * `numpy`
    * `scipy`
    * `matplotlib`

Other packages may be needed later on.